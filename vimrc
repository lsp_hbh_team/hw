" 검색어 하이라이트
set hlsearch
" 검색어 입력 시 위치 쫓아가기
set incsearch

" 자동 인덴트
set autoindent
" C indent
set cindent


" tabstop and shiftwidth
set ts=4
set shiftwidth=4

" 괄호 매치 하이라이트
set showmatch


