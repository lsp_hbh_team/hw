#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#define BUFF_SIZE 32

int main(int argc, char **argv) 
{
	char buff[BUFF_SIZE];
	int fd;
	int size;

	/* need to check argc */

	fd = open(argv[1], O_RDONLY);
	
	if(fd == -1) {
		perror("ERROR ");
		return -1;
	}

	/* need to check file type */

	while((size = read(fd, buff, BUFF_SIZE-1)) > 0) {
		buff[size] = '\0';
		fputs(buff, stdout);
	}

	close(fd);

	return 0;	
}
