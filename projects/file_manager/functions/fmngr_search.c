#include "fmngr.h"

int search_chdir(const char * path)
{
	if (chdir(path) == -1) {
		perror("chdir");
		return -1;
	}

	return 0;
}

int search_ls(char * curdir)
{
	DIR * dirp;
	struct dirent * dp;
	dirp = opendir(curdir);

	if (dirp == NULL) {
		perror("opendir");
		return -1;
	}

	while ((dp = readdir(dirp))) {
	       printf("%s\n",dp->d_name);
	}	      

	if (closedir(dirp) == -1) {
		perror("closedir");
		return -1;
	}	

	return 0;
}

int search_cat(char * fname)
{
	char buff[1024];
	memset(buff, 0, sizeof(buff));
	int ret;
	int fd;

	struct stat stats;
	stat(fname, &stats);

	if (S_ISDIR(stats.st_mode)) {
		printf("ERROR, %s is a directory.\n", fname);
		return 0;
	}

	fd = open(fname, O_RDONLY);
	if (fd == -1) {
		perror("open");
		return -1;
	}

	while((ret = read(fd, buff, sizeof(buff)))) {
		write(STDOUT_FILENO, buff, ret);
	}		

	close(fd);

	return 0;
		
}
