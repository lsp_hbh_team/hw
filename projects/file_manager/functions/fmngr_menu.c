#include "fmngr.h"

int printmenu(struct menu_selection * select)
{
	int i;
	int num = select->count;
	for (i = 0;i < num;i++) {
		printf("%d %s\n", i+1, select->menus[i].title);
	}

	return 0;
}

int addchild(int num, struct menu_selection * parent, struct menu_selection * child)
{
	if (num < 0) {
	       return -1;
	}	       

	parent->menus[num].submenu = child;
	child->parent = parent;
}

int node_search_chdir (struct menu_selection ** select)
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));

	printf("Please input a directory name : ");
	if (fgets(buf, sizeof(buf), stdin) == NULL) {
		printf("fgets ERROR");
		return -1;
	}

	if (buf[strlen(buf) - 1] == '\n') {
		buf[strlen(buf) - 1] = '\0';
	}

	if (search_chdir(buf) == -1) {
		printf("ERROR\n");
		return -1;
	} else {
		return 0;
	}
}

int node_search_ls (struct menu_selection ** select)
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));

	if (getcwd(buf, sizeof(buf)) == NULL) {
		perror("getcwd");
		return -1;
	}

	if (search_ls(buf) == -1) {
		printf("SEARCH LS ERROR");
		return -1;
	}

	return 0;
}


int node_search_cat (struct menu_selection ** select)
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));

	printf("Please input a file name : ");
	if (fgets(buf, sizeof(buf), stdin) == NULL) {
		printf("fgets ERROR");
		return -1;
	}

	if (buf[strlen(buf) - 1] == '\n') {
		buf[strlen(buf) - 1] = '\0';
	}

	if (search_cat(buf) == -1) {
		printf("ERROR\n");
		return -1;
	} else {
		return 0;
	}
}

int node_find_mv (struct menu_selection ** select)
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));

	printf("Please input a directory name : ");
	if (fgets(buf, sizeof(buf), stdin) == NULL) {
		printf("fgets ERROR");
		return -1;
	}

	if (buf[strlen(buf) - 1] == '\n') {
		buf[strlen(buf) - 1] = '\0';
	}

	if (find_mv(buf) == -1) {
		printf("ERROR\n");
		return -1;
	} else {
		return 0;
	}
}

int node_find_nsearch (struct menu_selection ** select)
{
	char input[1024];
	char curdir[1024];
	memset(input, 0, sizeof(input));
	memset(curdir, 0, sizeof(curdir));
	
	if (getcwd(curdir, sizeof(curdir)) == NULL) {
		perror("getcwd");
		return -1;
	}

	printf("Please input a file/directory name : ");
	if (fgets(input, sizeof(input), stdin) == NULL) {
		printf("fgets ERROR");
		return -1;
	}

	if (input[strlen(input) - 1] == '\n') {
		input[strlen(input) - 1] = '\0';
	}

	if (find_nsearch(input, curdir) == -1) {
		printf("NSEARCH ERROR");
		return -1;
	}

	return 0;
}

int node_find_ssearch (struct menu_selection ** select)
{
	int ineq_sign, size, menu_num;
	char curdir[1024];
	memset(curdir, 0, sizeof(curdir));
	
	if ((getcwd(curdir, sizeof(curdir))) == NULL) {
		perror("getcwd");
		return -1;
	}

	printf("Please input the size of the file : \n");
	scanf("%d", &size);
	printf("1. GT\n2. EQ\n3. LT\n");
	scanf("%d", &menu_num);
	if (menu_num == 1) {
		ineq_sign = GREATER_THAN;
	} else if (menu_num == 2) {
		ineq_sign = EQUAL_TO;
	} else if (menu_num == 3) {
		ineq_sign = LESS_THAN;
	} else {
		return -1;
	}

	find_ssearch(size, ineq_sign, curdir);
}

int node_moni_dir_add (struct menu_selection ** select)
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));

	printf("Please input a directory name : ");
	if (fgets(buf, sizeof(buf), stdin) == NULL) {
		printf("fgets ERROR");
		return -1;
	}

	if (buf[strlen(buf) - 1] == '\n') {
		buf[strlen(buf) - 1] = '\0';
	}

	
	if (moni_dir_add(buf, &glob) == -1) {
		printf("ERROR\n");
		return -1;
	} else {
		return 0;
	}

}

int node_moni_dir_rm (struct menu_selection ** select)
{
	char buf[1024];
	memset(buf, 0, sizeof(buf));

	printf("Please input a directory name : ");
	if (fgets(buf, sizeof(buf), stdin) == NULL) {
		printf("fgets ERROR");
		return -1;
	}

	if (buf[strlen(buf) - 1] == '\n') {
		buf[strlen(buf) - 1] = '\0';
	}

	if (moni_dir_rm(buf, &glob) == -1) {
		printf("ERROR\n");
		return -1;
	} else {
		return 0;
	}
}
int node_moni_dir_start (struct menu_selection ** select)
{
	moni_dir_start(&glob);
	return 0;
}

int node_moni_hist_ls (struct menu_selection ** select)
{
	int input;

	moni_dir_ls(&glob);
	
	printf("Please input directory number : \n");
	scanf("%d", &input);

	getchar();

	moni_hist_ls(&glob, input-1);	
	
	return 0;
}
int node_moni_hist_clr (struct menu_selection ** select)
{
	int input;

	moni_dir_ls(&glob);
	
	printf("Please input directory number : \n");
	scanf("%d", &input);

	getchar();

	moni_hist_clr(&glob, input-1);	

	return 0;
}
int node_moni_dir_ls (struct menu_selection ** select)
{
	moni_dir_ls(&glob);
	return 0;
}

int menu_return(struct menu_selection ** select)
	
{
	(*select) = (*select)->parent;
	return 0;
}
