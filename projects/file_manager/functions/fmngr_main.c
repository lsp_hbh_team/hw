#include "fmngr.h"

struct monitor_global glob = {
	.count = 0,
};

int main()
{
	int input;
	char buf[1024];

	glob.fd = inotify_init();

	struct menu_selection root = {
		.count = 3,
		.menus = {
			{
				.title = "Search",

			},
			{
				.title = "Find",

			},
			{
				.title = "Monitor",
			},
		},
	};

	struct menu_selection search = {
		.count = 4,
		.menus = {
			{
				.title = "Move the current directory",
				.func = node_search_chdir,
			},
			{
				.title = "List files & directories",
				.func = node_search_ls,
			},
			{
				.title = "Show file content",
				.func = node_search_cat,

			},
			{
				.title = "Return",
				.func = &menu_return,

			},
		},
	};

	struct menu_selection find = {
		.count = 4,	
		.menus = {
			{
				.title = "Move the base directory",
				.func = node_find_mv,

			},
			{
				.title = "Search by name",
				.func = node_find_nsearch,

			},
			{
				.title = "Search by size",
				.func = node_find_ssearch,
			},
			{
				.title = "Return",
				.func = &menu_return,

			},
		},
	};

	struct menu_selection monitor = {
		.count = 6,
		.menus = {
			{
				.title = "Add monitor",
				.func = node_moni_dir_add,

			},
			{
				.title = "Remove monitor",
				.func = node_moni_dir_rm,

			},
			{
				.title = "Start monitoring",
				.func = node_moni_dir_start,

			},
			{
				.title = "View history",

			},
			{
				.title = "List monitored directories",
				.func = node_moni_dir_ls,

			},
			{
				.title = "Return",
				.func = &menu_return,

			},
		},
	};

	struct menu_selection history_submenu = {
		.count = 3, 
		.menus = {
			{
				.title = "List history",
				.func = node_moni_hist_ls,

			},	
			{
				.title = "Clear history",
				.func = node_moni_hist_clr,

			},	
			{
				.title = "Return",
				.func = &menu_return,

			},	
		},
	};

	addchild(0, &root, &search);
	addchild(1, &root, &find);
	addchild(2, &root, &monitor);
	addchild(3, &monitor, &history_submenu);

	struct menu_selection * curmenu = &root; 

	while(1) {
		printmenu(curmenu);
		memset(buf, 0, sizeof(buf));
		fgets(buf, sizeof(buf), stdin);
		sscanf(buf, "%d", &input);

		if (curmenu->menus[input-1].submenu == NULL) {
			if (curmenu->menus[input-1].func == NULL) {
				printf("ERROR, NO ASSIGNED FUNCTION\n");
				continue;
			}
			
			if (curmenu->menus[input-1].func(&curmenu) == -1) {
				printf("FUNCTION ERROR(%s()@%s %d line)\n", __func__, __FILE__, __LINE__);
			}

		} else {
			curmenu = curmenu->menus[input-1].submenu;
		}
	}

	close(glob.fd);
}
