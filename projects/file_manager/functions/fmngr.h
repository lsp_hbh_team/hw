#ifndef FMNGR_H
#define FMNGR_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/inotify.h>
#include <time.h>
#include <signal.h>
#include <errno.h>

#include "fmngr_search.h"
#include "fmngr_find.h"
#include "fmngr_monitor.h"
#include "fmngr_menu.h"

struct monitor_global glob;

#endif /* FMNGR_H */
