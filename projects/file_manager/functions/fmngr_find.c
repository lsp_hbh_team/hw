#include "fmngr.h"

int find_mv(char * dirname)
{
	struct stat stats;

	lstat(dirname, &stats);

	if (!S_ISDIR(stats.st_mode)) {
		printf("ERROR, %s is not a directory.\n", dirname);
		return -1;
	}

	if (chdir(dirname) == -1) {
		perror("chdir");
		return -1;
	}

	return 0;
}

int find_nsearch(char * fname, char * curdir)
{
	DIR * dirp;
	struct dirent * dp;
	char * ret;

	dirp = opendir(curdir);

	if (dirp == NULL) {
		perror("opendir");
		return -1;
	}

	while ((dp = readdir(dirp))) {

		if (strcmp(".", dp->d_name) == 0 || strcmp("..", dp->d_name) == 0) {
			continue;
		}

		char searchdir[strlen(curdir) + strlen(dp->d_name) +2];
		strcpy(searchdir, curdir);
		strcat(searchdir, "/");
		strcat(searchdir, dp->d_name);

		struct stat stats;
		stat(searchdir, &stats);
	       
		if (strcmp(fname, dp->d_name) == 0) {
		       printf("%s found in %s\n", fname, curdir);
	       	} else if ((ret = strstr(dp->d_name, fname))) {
		        printf("%s found in %s\n", dp->d_name, curdir);
		}
		
		if (S_ISDIR(stats.st_mode)) {
		       if (find_nsearch(fname, searchdir) == -1) {
			       printf("NSEARCH ERROR\n");
		       }
	       	}
	}	      

	if (closedir(dirp) == -1) {
		perror("closedir");
		return -1;
	}	

	return 0;
}	

int find_ssearch(int size, int ineq_sign, char * curdir)
{
	DIR * dirp;
	struct dirent * dp;
	char * ret;

	dirp = opendir(curdir);

	if (dirp == NULL) {
		perror("opendir");
		return -1;
	}

	while ((dp = readdir(dirp))) {

		if (strcmp(".", dp->d_name) == 0 || strcmp("..", dp->d_name) == 0) {
			continue;
		}

		char searchdir[strlen(curdir) + strlen(dp->d_name) +2];
		strcpy(searchdir, curdir);
		strcat(searchdir, "/");
		strcat(searchdir, dp->d_name);

		struct stat stats;
		stat(searchdir, &stats);
	
		
		if (ineq_sign == GREATER_THAN) {
			if (stats.st_size > size) {
				printf("%s is found at %s\n", dp->d_name, curdir);
			}		
		} else if (ineq_sign == EQUAL_TO) {
			if (stats.st_size == size) {
				printf("%s is found at %s\n", dp->d_name, curdir);
			}		
		} else if (ineq_sign == LESS_THAN) {
			if (stats.st_size < size) {
				printf("%s is found at %s\n", dp->d_name, curdir);
			}		
		}	
		
		if (S_ISDIR(stats.st_mode)) {
		       if (find_ssearch(size, ineq_sign, searchdir) == -1) {
			       printf("NSEARCH ERROR\n");
		       }
	       	}
	}	      

	if (closedir(dirp) == -1) {
		perror("closedir");
		return -1;
	}	

	return 0;
}
