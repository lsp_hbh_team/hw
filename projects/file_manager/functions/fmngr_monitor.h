#ifndef MONITOR_H
#define MONITOR_H

#define GREATER_THAN 1
#define EQUAL_TO 2
#define LESS_THAN 3

#define CREATE 0
#define DELETE 1

#define MAX_EVENTS 10

struct monitor_events {
	time_t event_time;
	char file_name[1024];
	int event_type;
};

struct monitor_list {
	int wd;
	int index;
	int count;
	struct monitor_events events[MAX_EVENTS];
	char dirname[1024];
};

struct monitor_global {
	int count;
	int fd;
	struct monitor_list mon_list[MAX_EVENTS];
};

int moni_dir_add(char * dir_name, struct monitor_global * monitors);

int moni_dir_rm(char * dir_name, struct monitor_global * monitors);

int moni_dir_ls(struct monitor_global * monitors); 

void sig_handler(int signo);

int moni_dir_start(struct monitor_global * monitors);

int moni_hist_ls(struct monitor_global * monitors, int dir_num);

int moni_hist_clr(struct monitor_global * monitors, int dir_num);

#endif /* endif monitor_h */


