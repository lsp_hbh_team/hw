#ifndef MENU_H
#define MENU_H

struct menu_item {
	char title[60];
	struct menu_selection * submenu;
	int (*func)(struct menu_selection **);
};

struct menu_selection {
	int count;
	struct menu_selection * parent;
	struct menu_item menus[10];
};

int printmenu(struct menu_selection * select);

int addchild(int num, struct menu_selection * parent, struct menu_selection * child);

int node_search_chdir (struct menu_selection ** select);

int node_search_ls (struct menu_selection ** select);

int node_search_cat (struct menu_selection ** select);

int node_find_mv (struct menu_selection ** select);

int node_find_nsearch (struct menu_selection ** select);

int node_find_ssearch (struct menu_selection ** select);

int node_moni_dir_add (struct menu_selection ** select);

int node_moni_dir_rm (struct menu_selection ** select);

int node_moni_dir_ls (struct menu_selection ** select);

int node_moni_dir_start (struct menu_selection ** select);

int node_moni_hist_ls (struct menu_selection ** select);

int node_moni_hist_clr (struct menu_selection ** select);

int menu_return(struct menu_selection ** select);

#endif /* endif menu.h */

