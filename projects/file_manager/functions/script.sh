#!/bin/bash

cd test1
touch 1-a
touch 1-b
touch 1-c
rm 1-a
rm 1-b
rm 1-c
cd ..

cd test2
touch 2-a
touch 2-b
touch 2-c
rm 2-a
rm 2-b
rm 2-c
cd ..

cd test3
touch 3-a
touch 3-b
touch 3-c
rm 3-a
rm 3-b
rm 3-c
cd ..
