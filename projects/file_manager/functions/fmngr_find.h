#ifndef FIND_H
#define FIND_H

int find_mv(char * dirname);

int find_nsearch(char * fname, char * curdir);

int find_ssearch(int size, int ineq_sign, char * curdir);

#endif /* endif find_h */
