#include "fmngr.h"

int sig_input = 1;

int moni_dir_add(char * dir_name, struct monitor_global * monitors)
{
	int wd, count, i;
	count = monitors->count;

	struct stat stats;

	if (stat(dir_name, &stats) == -1) {
		perror("stat");
		return -1;
	}

	if (S_ISDIR(stats.st_mode) == 0) {
		printf("ERROR, %s is not a directory.\n", dir_name);
		return -1;
	}

	for (i = 0; i < count; i++) {
		if (strcmp(monitors->mon_list[i].dirname, dir_name) == 0) {
			printf("ERROR, directory is already being monitored!\n");
			return -1;
		}
	}

	wd = inotify_add_watch(monitors->fd, dir_name, IN_CREATE | IN_DELETE);
	
	monitors->mon_list[count].wd = wd;
	strcpy(monitors->mon_list[count].dirname, dir_name);

	monitors->mon_list[count].index = 0;
	monitors->mon_list[count].count = 0;

	monitors->count = monitors->count + 1;

	return 0;
}

int moni_dir_rm(char * dir_name, struct monitor_global * monitors)
{
	int i, j;
	int count = monitors->count;
	int delet = -1;

	struct stat stats;
	if (stat(dir_name, &stats) == -1) {
		perror("stat");
		return -1;
	}
	
	if (S_ISDIR(stats.st_mode) == 0) {
		printf("ERROR, %s is not a directory.\n", dir_name);
		return -1;
	}

	for (i = 0; i < count; i++) {
		if (strcmp(monitors->mon_list[i].dirname, dir_name) == 0) {
			inotify_rm_watch(monitors->fd, monitors->mon_list[i].wd);
			memset(&monitors->mon_list[i], 0, sizeof(struct monitor_list));
			delet = i;
			break;
		}
	}		

	
	if (delet != -1) {
		if (count != delet + 1) {
			memmove(monitors->mon_list + delet, monitors->mon_list + delet + 1, sizeof(struct monitor_list) * (count - 1 - delet));
		} 
		monitors->count = monitors->count - 1;
	}
}

int moni_dir_ls(struct monitor_global * monitors) 
{
	int i;
	printf("Count is : %d\n", monitors->count);
	
	for (i = 0; i < monitors->count; i++) {
		printf("%d. Directory : %s\n", i+1, monitors->mon_list[i].dirname);
	}

	return 0;
}

void sig_handler(int signo)
{
	if (signo == SIGINT) {
		printf("Caught signal!\n");
	}

}

int moni_dir_start(struct monitor_global * monitors)
{
	int ret, i, type, index, dir_num;
	char buf[1024];
	char * ptr;
	int input = 1;
	int dir_count = monitors->count;

	struct inotify_event * evnt;

	struct sigaction sig_act = {.sa_handler = sig_handler};
	sigaction(SIGINT, &sig_act, NULL);

	while(input) {
		ret = read(monitors->fd, buf, sizeof(buf));
	
		if (ret < 0) {
			if (errno == EINTR) {
				input = 0;
				break;
			} else {
			       perror("read");
		       	       return -1;
			}	       
		}

		for (ptr = buf; ptr < buf + ret; ptr += sizeof(buf) + evnt->len) {
			
			evnt = (struct inotify_event * ) ptr;

			for (i = 0; i < dir_count; i++) {
				if (evnt->wd == monitors->mon_list[i].wd) {
					dir_num = i;
				}
			}

			if (evnt->len) {
				if (evnt->mask & IN_CREATE) {
					type = CREATE;
				} else if (evnt->mask & IN_DELETE) {
					type = DELETE;
				} else {
					continue;
				}

				index = monitors->mon_list[dir_num].index;
				time(&monitors->mon_list[dir_num].events[index].event_time);
				strcpy(monitors->mon_list[dir_num].events[index].file_name, evnt->name);

				monitors->mon_list[dir_num].events[index].event_type = type;

				printf("DIR NUM : %d\n", dir_num);
				printf("The %s %s was %s in %s\n", (evnt->mask & IN_ISDIR)? "directory" : "file" , evnt->name, (type == CREATE)? "created" : "deleted"  , monitors->mon_list[dir_num].dirname);
				monitors->mon_list[dir_num].count++;
				monitors->mon_list[dir_num].index++;
				if (monitors->mon_list[dir_num].index == MAX_EVENTS) {
					monitors->mon_list[dir_num].index = 0;
				}
			}
		}
	}

	struct sigaction sig_act2 = {.sa_handler = SIG_DFL};
	sigaction(SIGINT, &sig_act2, NULL);

	return 0;

}

int moni_hist_ls(struct monitor_global * monitors, int dir_num)
{
	int i, type;
	time_t date;
	char buff[20];
	int count = monitors->mon_list[dir_num].count;
	int index = monitors->mon_list[dir_num].index;

	if (count > MAX_EVENTS) {
		for (i = index; i < MAX_EVENTS; i++) {
			date = monitors->mon_list[dir_num].events[i].event_time;
			type = monitors->mon_list[dir_num].events[i].event_type;
			strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&date)); 
			
			printf("%s %s was %s in %s.\n", buff, monitors->mon_list[dir_num].events[i].file_name , (type == CREATE)? "created" : "deleted", monitors->mon_list[dir_num].dirname);	
		}
		
		for (i = 0; i < index; i++) {
			date = monitors->mon_list[dir_num].events[i].event_time;
			type = monitors->mon_list[dir_num].events[i].event_type;
			strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&date)); 
			
			printf("%s %s was %s in %s.\n", buff, monitors->mon_list[dir_num].events[i].file_name , (type == CREATE)? "created" : "deleted", monitors->mon_list[dir_num].dirname);	
		}
	} else {
		for (i	= 0; i < count; i++) {
			date = monitors->mon_list[dir_num].events[i].event_time;
			type = monitors->mon_list[dir_num].events[i].event_type;
			strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&date)); 
			
			printf("%s %s was %s in %s.\n", buff, monitors->mon_list[dir_num].events[i].file_name , (type == CREATE)? "created" : "deleted", monitors->mon_list[dir_num].dirname);	
		}
	}

	return 0;
}

int moni_hist_clr(struct monitor_global * monitors, int dir_num) 
{
	monitors->mon_list[dir_num].count = 0;
	monitors->mon_list[dir_num].index = 0;

	memset(&monitors->mon_list[dir_num].events, 0, sizeof(monitors->mon_list[dir_num].events));

	return 0;
}
