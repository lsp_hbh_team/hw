#include "common.h"
#include <pthread.h>

int file_get(int client_fd, char * path)
{
	struct data_header header;
	ssize_t ret, data_read;
	int fd;
	off_t size;
	struct stat statbuf;

	data_read = 0;

	printf("%s\n", path);

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		perror("open");
		return -1;
	}

	if (fstat(fd, &statbuf) == -1) {
		perror("fstat");
		return -1;
	}
	
	size = statbuf.st_size;

	while(data_read < size) {
		ret = read(fd, header.data, sizeof(header.data));
		if (ret == -1) {
		       perror("read");
		       return -1;
		}	       

		data_read += ret;
		header.length = ret;
	
		printf("RET IS : %ld\n", ret);

		if (send(client_fd, &header, sizeof(struct data_header), 0) == -1) {
			perror("send");
			return -1;
		}
	}

	header.length = 0;			
	if (send(client_fd, &header, sizeof(struct data_header), 0) == -1) {
		perror("send");
		return -1;
	}

	printf("EOF\n");

	close(fd);

	return 0;
}

int func_get(int port, char * path)
{
	int client_fd;
	struct sockaddr_in clientaddr;
	socklen_t addrlen;

	addrlen = sizeof(struct sockaddr_in);

	client_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (client_fd == -1) {
		perror("socket");
		return -1;
	}

	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(port);
	if (inet_aton("127.0.0.1", &clientaddr.sin_addr) == 0) {
		perror("inet_aton");
		return -1;
	}

	if (connect(client_fd, (struct sockaddr *)&clientaddr, addrlen) == -1) {
		perror("connect");
		return -1;
	}

	if (file_get(client_fd, path) == -1) {
		printf("file_get error\n");
		return -1;
	}


	if (close(client_fd) == -1) {
		perror("close");
		return -1;
	}

	return 0;
}

void * server_main(void * client) 
{	
	int fd = *((int *) client);
	struct request_header request;
	ssize_t ret;

	printf("THREAD CREATED!\n");

	ret = recv(fd, &request, sizeof(request), 0);
	if (ret == -1) {
		perror("recv");
		return NULL;
	}

	printf("PORT NUMBER : %d\n", request.port);

	printf("REQUEST HEADER RECEIVED\n");

	if (request.cmd == GET) {
	        printf("RECEIVED THE COMMAND GET\n");	
		if (func_get(request.port, request.parameter) == -1) {
			printf("func_get error\n");
			return NULL;
		}			
	}

	printf("AAA\n");

	if (close(fd) == -1) {
		perror("close");
		return NULL;
	}

	return NULL;
}


int main()
{
	int server_fd, client_fd;
	struct sockaddr_in serveraddr, clientaddr;
	socklen_t addrlen;
	pthread_t thread;
	int enable = 1;

	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd == -1) {
		perror("socket");
		return 1;
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1) {
		perror("setsockopt");
		return 1;
	}

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(60000);
	if (inet_aton("127.0.0.1", &serveraddr.sin_addr) == 0) {
		perror("inet_aton");
		return 1;
	}

	addrlen = sizeof(struct sockaddr_in);

	if (bind(server_fd, (struct sockaddr *)&serveraddr, addrlen) == -1) { 
		perror("bind");
		return 1;
	}

	if (listen(server_fd, 1) == -1) {
		perror("connect");
		return 1;
	}

	while(1) {
		client_fd = accept(server_fd, (struct sockaddr *)&clientaddr, &addrlen);
		if (client_fd == -1) {
			perror("accept");
			return 1;
		}

		pthread_create(&thread, NULL, server_main, (void *) &client_fd);
	}


	return 0;
}
