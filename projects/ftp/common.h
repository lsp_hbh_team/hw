#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define CD 0
#define PUT 1
#define GET 2
#define LS 3
#define DEL 4
#define QUIT 5

struct request_header {
	int cmd;
	int port;
	char parameter[1024];
};

struct response_header {
	int cmd;
	int length;
	char error[1024];
};

struct data_header {
	int length;
	char data[1024];
};
