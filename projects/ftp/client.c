#include "common.h"

int data_open(int port)
{
	int fd, fd2;
	int enable = 1;
	socklen_t addrlen;
	struct sockaddr_in serveraddr, clientaddr;

	printf("DATA OPEN FUNCTION\n");

	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd == -1) {
		perror("socket");
		return 1;
	}

	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1) {
		perror("setsockopt");
		return 1;
	}

	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(port);
	if (inet_aton("127.0.0.1", &clientaddr.sin_addr) == 0) {
		perror("inet_aton");
		return 1;
	}

	addrlen = sizeof(struct sockaddr_in);

	if (bind(fd, (struct sockaddr *)&clientaddr, addrlen) == -1) { 
		perror("bind");
		return 1;
	}

	printf("BINDING...\n");

	if (listen(fd, 1) == -1) {
		perror("connect");
		return 1;
	}

	printf("LISTENING...\n");

	fd2 = accept(fd, (struct sockaddr *)&serveraddr, &addrlen); 
	if (fd2 == -1) {
		perror("connect");
		return 1;
	}

	printf("ACCEPTING...\n");

	return fd2;
}

int data_get(int fd, struct request_header * request) {

	int data_fd, write_fd;	
	struct data_header header;
	ssize_t ret, data_read;

	printf("DATA GET FUNCTION\n");

	write_fd = open(request->parameter, O_CREAT | O_WRONLY, 0600);
	if (write_fd == -1) {
		perror("open");
		return -1;
	}

	if (send(fd, request, sizeof(struct request_header), 0) == -1) {
		perror("send");
		return -1;		
	}

	data_fd = data_open(request->port);

	do {
		ret = read(data_fd, &header, sizeof(struct data_header));
		if (ret == -1) {
			perror("send");
			return -1;		
		}

		printf("RET IS : %ld\n", ret);

		data_read += ret;

		if (header.length != 0) {
			ret = write(write_fd, header.data, header.length);
			if (ret == -1) {
				perror("write");
				return -1;
			}
		}
	} while(header.length > 0);

	close(write_fd);
	close(data_fd);

	return 0;
}

int parsing(int fd)
{
	size_t buf_len;
	struct request_header request;
	char buf[1024];
	char cmd[10];
	char * token;

	buf_len = sizeof(buf);

	fgets(buf, buf_len, stdin);
	if (buf[strlen(buf) - 1] == '\n') {
		buf[strlen(buf) - 1] = '\0';
	}

	printf("FROM STDIN : %s\n", buf);

	token = strtok(buf, " ");
	if (token == NULL) {
		printf("Please try again\n");
		return 1;
	}

	strcpy(cmd, token);
	printf("AFTER PARSING : %s\n", cmd);

	if (strcmp(cmd, "cd") == 0) {
		request.cmd = CD;
	} else if (strcmp(cmd, "get") == 0) {
		request.cmd = GET;
	} else if (strcmp(cmd, "put") == 0) {
		request.cmd = PUT;
	} else if (strcmp(cmd, "ls") == 0) {
		request.cmd = LS;
	} else if (strcmp(cmd, "del") == 0) {
		request.cmd = DEL;
	} else if (strcmp(cmd, "quit") == 0) {
		request.cmd = QUIT;
	} else {
		printf("Unknown command. Please try again\n");
		return 1;
	}

	token = strtok(NULL, " ");
	if (token == NULL) {
		printf("Please try again\n");
		return 1;
	}

	request.port = atoi(token);
	
	printf("PORT NUMBER : %d\n", request.port);

	token = strtok(NULL, " ");
	if (token == NULL) {
		printf("Please try again\n");
		return 1;
	}

	strncpy(request.parameter, token, sizeof(request.parameter) - 1);

	if (request.cmd == GET) {
		data_get(fd, &request);
	}

	return 0;
}


int main()
{
	int fd;
	struct sockaddr_in serveraddr;
	socklen_t addrlen;

	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd == -1) {
		perror("socket");
		return 1;
	}

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(60000);
	if (inet_aton("127.0.0.1", &serveraddr.sin_addr) == 0) {
		perror("inet_aton");
		return 1;
	}	

	addrlen = sizeof(struct sockaddr_in);

	if (connect(fd, (struct sockaddr *)&serveraddr, addrlen) == -1) {
		perror("connect");
		return 1;
	}

	parsing(fd);


	/*

	token = strtok(NULL, " ");
	if (token == NULL) {
		printf("Please try again\n");
		return 1;
	}

	strcpy(buf2, token);
	request.port = atoi(buf2);
	token = strtok(NULL, " ");
	if (token == NULL) {
		printf("Please try again\n");
		return 1;
	}

	strcpy(buf2, token);
	strcpy(request.parameter, buf2);
	token = strtok(NULL, " ");
	if (token != NULL) {
		printf("Please try again\n");
		return 1;
	}

	ret = send(fd, &request, sizeof(request), 0);
	if (ret == -1) {
		perror("send");
		return 1;
	}

	fd2 = socket(AF_INET, SOCK_STREAM, 0);
	if (fd2 == -1) {
		perror("socket");
		return 1;
	}

	if (setsockopt(fd2, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1) {
		perror("setsockopt");
		return 1;
	}

	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(request.port);
	if (inet_aton("127.0.0.1", &clientaddr.sin_addr) == 0) {
		perror("inet_aton");
		return 1;
	}

	if (bind(fd2, (struct sockaddr *)&clientaddr, addrlen) == -1) { 
		perror("bind");
		return 1;
	}

	printf("BINDING...\n");

	if (listen(fd2, 1) == -1) {
		perror("connect");
		return 1;
	}

	fd3 = accept(fd2, (struct sockaddr *)&serveraddr, &addrlen); 
	if (fd3 == -1) {
		perror("connect");
		return 1;
	}

	printf("ACCEPTING...\n");

	ret = recv(fd3, buf2, buf_len, 0);
	if (ret == -1) {
		perror("recv");
		return 1;
	}

	printf("FROM SERVER : %s\n", buf2);

	return 0;
	*/
}
