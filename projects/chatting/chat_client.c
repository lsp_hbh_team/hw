#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <string.h>
#include "chat_msg.h"

#define MAXLINE 1024

int sock_send(int fd, void *buf, size_t len) 
{
	int ret;
	int saved = 0;
	char *p = buf;

	while (saved < len) {
		ret = send(fd, p + saved, len - saved, 0);

		if (ret == -1) {
			perror("send ");
			return -1;
		} else if (ret > 0) {
			saved += ret;
			continue;
		} else if (ret == 0) {
			return -1;
		}	
	}

	return 0;
}

int sock_read(int fd, void *buf, size_t len) 
{
	int ret;
	int saved = 0;
	char *p = buf;

	while (saved < len) {
		ret = recv(fd, p + saved, len - saved, 0);

		if (ret == -1) {
			perror("send ");
			return -1;
		} else if (ret > 0) {
			saved += ret;
			continue;
		} else if (ret == 0) {
			return -1;
		}	
	}

	return 0;
}

int main(int argc, char **argv)
{

	struct header client_header; 

	int client_len;
	int client_sockfd;
	int efd, event_fd;
	int ret;

	char buf_in[MAXLINE];
	char buf_get[MAXLINE];

	struct sockaddr_un clientaddr;
	struct epoll_event p_events;

	efd = epoll_create(1); 


	if (argc != 3) {
		printf("Usage : %s [file_name] [id]\n", argv[0]);
		return -1;		
	}

	if (strlen(argv[2]) > ID_LENGTH) {
	       printf("Max id length is %d\n", ID_LENGTH);
	       return -1;
	}


	client_sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (client_sockfd == -1) {
		perror("error : ");
		return -1;
	}

	bzero(&clientaddr, sizeof(clientaddr));
	clientaddr.sun_family = AF_UNIX;
	strcpy(clientaddr.sun_path, argv[1]);
	client_len = sizeof(clientaddr);

	p_events.data.fd = client_sockfd;
	p_events.events = EPOLLIN;
	epoll_ctl(efd, EPOLL_CTL_ADD, client_sockfd, &p_events);	

	p_events.data.fd = 0;
	p_events.events = EPOLLIN;
	epoll_ctl(efd, EPOLL_CTL_ADD, 0, &p_events);	

	if (connect(client_sockfd, (struct sockaddr *)&clientaddr, client_len) < 0) {
		perror("Connect error : ");
		return -1;
	}

	client_header.magic = KEY;
	client_header.tag = REGISTER;
	client_header.length = strlen(argv[2]);

	ret = sock_send(client_sockfd, &client_header, sizeof(client_header));
	if (ret == -1) {
		printf("ERROR, SOCK_SEND");
		return 1;
	}
	
	ret = sock_send(client_sockfd, argv[2], client_header.length);
	if (ret == -1) {
		printf("ERROR, SOCK_READ");
		return 1;
	}

	while(1)
	{
		if (epoll_wait(efd, &p_events, 1, -1) == -1) {
			perror("epoll_wait");
			return -1;
		}

		event_fd = p_events.data.fd;

		memset(buf_in, 0x00, MAXLINE);
		memset(buf_get, 0x00, MAXLINE);
		printf("> %d\n", event_fd);

		if (event_fd == STDIN_FILENO) {
			fgets(buf_in, sizeof(buf_in), stdin);	

			client_header.magic = KEY;
			client_header.tag = MESSAGE;
			client_header.length = strlen(buf_in);

			printf("%s\n", buf_in);

			ret = sock_send(client_sockfd, &client_header, sizeof(client_header));
			if (ret == -1) {
				printf("ERROR, SOCK_SEND");
				return 1;
			}

			ret = sock_send(client_sockfd, buf_in, client_header.length);
			if (ret == -1) {
				printf("ERROR, SOCK_READ");
				return 1;
			}
	
			printf("%s\n", buf_in);

			continue;
		} else if (event_fd == client_sockfd) {
			ret = sock_read(client_sockfd, &client_header, sizeof(client_header));
			if (ret == -1) {
				printf("ERROR, SOCK READ\n");
				return -1;
			}

			ret = sock_read(client_sockfd, buf_get, client_header.length);
			if (ret == -1) {
				perror("read");
				return -1;
			}

			printf("-> %s", buf_get);

		} else {
			printf("ERR, EVENT FD\n");
			return -1;
		}
	}

	close(client_sockfd);
	
	return 0;
}

		
