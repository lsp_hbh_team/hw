#ifndef CHAT_MENU_H
#define CHAT_MENU_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/epoll.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <time.h>

#define MAXLINE 1024
#define MAXUSER 10
#define EMPTY (-1)

struct menu_item {
	char title[60];
	struct menu_selection * submenu;
	int (*func)(struct menu_selection **);
};

struct menu_selection {
	int count;
	struct menu_selection * parent;
	struct menu_item menus[10];
};

struct user_info {
	int sock_fd;	
	time_t join_time;
	time_t activity;
	char id[ID_LENGTH];
};

struct user_record {
	int count;
	struct user_info users[MAXUSER];
};

int menu_print(struct menu_selection * select);
int menu_processing(void);
int user_printf(struct user_record * record);
int user_del(struct user_record * record, int fd);
int user_lookup_fd(struct user_record * record, int fd);
int user_lookup_id(struct user_record * record, char * id);
int parse_msg(char *buf, int sender_fd);
int user_count(struct user_record * record);
int user_kick(int client_fd);

extern struct menu_selection * curmenu;
extern struct user_record record;
extern int efd;

#endif
