#include "chat_msg.h"
#include "chat_menu.h"

struct user_record record;
int efd;

int record_init(struct user_record * record)
{
	record->count = 0;
	int i;
	for (i = 0; i < MAXUSER; i++) {
		record->users[i].sock_fd = EMPTY;
	}

	return 0;
}

int user_get_slot(struct user_record * record)
{
	int i;

	for (i = 0; i < MAXUSER; i++) {
		if (record->users[i].sock_fd == EMPTY) {
			return i;
		}
	}	

	return -1;
}

int user_lookup_fd(struct user_record * record, int fd)
{
	int i;
	for (i = 0;i < MAXUSER;i++) {
		if (fd == record->users[i].sock_fd) {
			return i;
		}
	}

	return -1;
}

int user_lookup_id(struct user_record * record, char * id) {
	int i;
	for (i = 0; i < MAXUSER; i++) {
		if (strcmp(id, record->users[i].id) == 0) {
			return i;
		}
	}

	return -1;
}

int user_update(int fd)
{
	int index;
	index = user_lookup_fd(&record, fd);

	if (index == -1) {
		printf("User look up fd ERROR\n");
		return -1;
	}

	record.users[index].activity = time(NULL);

	return 0;
}

int user_timeout() 
{
	int count = user_count(&record);
	int i;
	time_t current;

	current = time(NULL);

	for (i = 0; i < count; i++) {
		if (current - record.users[i].activity == 60) {
			if (user_kick(record.users[i].sock_fd) == -1) {
				printf("User kick ERROR\n");
				return -1;
			}
		}
	}

	return 0;
}

int user_add(struct user_record * record, int fd, char * id)
{
	int slot, i;
	int length = strlen(id);

	if (strcmp(id, "bdcast") == 0) {
		printf("Error, reserved name, please try again\n");
		return -1;
	}

	for (i = 0; i < length; i++) {
		if (id[i] == '#') {
			printf("Error, please refrain from using # in your id\n");
			return -1;
		}
	}

	if (user_lookup_id(record, id) != -1) {
		printf("User of the same id already exits!\n");
		return -1;
	}	

	slot = user_get_slot(record);

	if (slot != -1) {
		record->users[slot].sock_fd = fd;
		strcpy(record->users[slot].id, id);
		record->users[slot].join_time = time(NULL);
		record->users[slot].activity = record->users[slot].join_time;
		record->count++;
		return 0;
	}
	
	printf("User limit reached!\n");

	return -1;
}

int user_del(struct user_record * record, int fd)
{
	int slot;
	slot = user_lookup_fd(record, fd);

	if (slot != -1) {
		record->users[slot].sock_fd = EMPTY;
		memset(record->users[slot].id, 0, sizeof(record->users[slot].id));
		record->count--;
		return 0;
	}

	return -1;
}

int sock_read(int fd, void *buf, size_t len)
{
	int ret;
	char *p = buf;
	int saved = 0;

	while(saved < len) {
		ret = recv(fd, p + saved, len - saved, 0);

		if (ret == -1) {
			perror("recv ");
			return -1;
		} else if (ret > 0) {
			saved += ret;
			continue;
		} else if (ret == 0) {
			return -1;	
		}
	}
	
	return 0;
}

int main(int argc, char **argv)
{
	int server_sockfd, client_sockfd;
	int state;
	socklen_t client_len;
	int event_fd;
	ssize_t recv_ret;
	char buf[MAXLINE];
	char msg[] = "A user of the same ID already exists, please try again\n";

	struct sockaddr_un clientaddr, serveraddr;
	struct epoll_event p_events;
        struct header client_header;	

	record_init(&record);

	if (argc != 2) {
		printf("Usage : %s [socket file name]\n", argv[0]);
		return -1;
	}
	
	efd = epoll_create(1);
	if (efd < 0) {
		perror("epoll create ");
		return 1;
	}

	if (access(argv[1], F_OK) == 0) {
		unlink(argv[1]);
	}

	client_len = sizeof(clientaddr);
	if ((server_sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("socket error : ");
		exit(0);
	}

	bzero(&serveraddr, sizeof(serveraddr));
	serveraddr.sun_family = AF_UNIX;
	strcpy(serveraddr.sun_path, argv[1]);

	state = bind(server_sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
	if (state == -1) {
		perror("bind error : ");
		exit(0);
	}

	state = listen(server_sockfd, 5);
	if (state == -1) {
		perror("listen error : ");
		exit(0);
	}

	p_events.events = EPOLLIN;
	p_events.data.fd = STDIN_FILENO;
	if (epoll_ctl(efd, EPOLL_CTL_ADD, STDIN_FILENO, &p_events) == -1) {
		perror("epoll_ctl ");
		return 1;
	}

	p_events.events = EPOLLIN;
	p_events.data.fd = server_sockfd;
	if (epoll_ctl(efd, EPOLL_CTL_ADD, server_sockfd, &p_events) == -1) {
		perror("epoll_ctl ");
		return 1;
	}

	menu_print(curmenu);

	p_events.data.fd = -1;

	while(1) {
		if (epoll_wait(efd, &p_events, 1, 1000) == -1) {
			perror("epoll wait ");
			return -1;
		}	

		if (user_timeout() == -1) {
			printf("User timeout ERROR\n");
			return 1;
		}

		event_fd = p_events.data.fd;

		if (event_fd == -1) {
			continue;
		}

		if (event_fd == server_sockfd) {
			client_sockfd = accept(server_sockfd, (struct sockaddr *)&clientaddr, &client_len);
			recv_ret = sock_read(client_sockfd, &client_header, sizeof(client_header));
			if (recv_ret == -1) {
				printf("RECV ERROR\n");
				close(client_sockfd);
				p_events.data.fd = -1;
				continue;
			}

			char id[client_header.length + 1];

			printf("LENGTH : %d\n", client_header.length);

			recv_ret = sock_read(client_sockfd, &id, client_header.length);
			if (recv_ret == -1) {
				printf("RECV ERROR\n");
				close(client_sockfd);
				p_events.data.fd = -1;
				continue;
			}

			id[client_header.length] = '\0';
		
			printf("USER ID : %s\n", id);

			if (user_add(&record, client_sockfd, id) == -1) {
				printf("user add error\n");
				write(client_sockfd, msg, sizeof(msg)); 
				close(client_sockfd);
				p_events.data.fd = -1;
				continue;
			}

			p_events.events = EPOLLIN | EPOLLRDHUP;
			p_events.data.fd = client_sockfd;
			printf("IF SERVER\n");
			printf("CLIENT : %d\n", client_sockfd);
			
			if (epoll_ctl(efd, EPOLL_CTL_ADD, client_sockfd, &p_events) == -1) {
				perror("epoll_ctl ");
				return 1;
			}

			p_events.data.fd = -1;

			continue;
		}	

		if (event_fd == STDIN_FILENO) {
			/*
			memset(buf, 0x00, MAXLINE);
			ret = read(event_fd, buf, MAXLINE);
			if (ret == -1) {
				perror("read ");
				return 1;
			}

			printf("%s\n", buf);
			*/
			/* process menu items. */
			menu_processing();
			p_events.data.fd = -1;
			continue;
		}

		memset(buf, 0x00, MAXLINE);
		recv_ret = sock_read(event_fd, &client_header, sizeof(client_header));

		printf("RECV_RET : %ld\n", recv_ret);

		if (recv_ret == -1) {
			printf("CLIENT : %d\n", event_fd);
			printf("IF RET <= 0\n");
			if (epoll_ctl(efd, EPOLL_CTL_DEL, event_fd, NULL) == -1) {
				perror("epoll_ctl ");
				return 1;
			}

			if (user_del(&record, event_fd) == -1) {
				printf("ERROR user_del\n");
				/*
				 * Fall through
				 */
			}

			close(event_fd);

			p_events.data.fd = -1;

		} else if (recv_ret == 0) {
			printf("READ\n");
			if (user_update(event_fd) == -1) {
				printf("User update ERROR\n");
				return -1;
			}

			if (client_header.tag == MESSAGE) {
				recv_ret = sock_read(event_fd, buf, client_header.length);
				if (recv_ret == -1) {
					printf("RECV ERROR\n");
					if (epoll_ctl(efd, EPOLL_CTL_DEL, event_fd, NULL) == -1) {
						perror("epoll_ctl ");
						return 1;
					}

					if (user_del(&record, event_fd) == -1) {
						printf("ERROR user_del\n");
						/*
						 * Fall through
						 */
					}

					close(event_fd);

					p_events.data.fd = -1;
					continue;
				}
				
				printf("USER MESSAGE : %s\n", buf);

				if (parse_msg(buf, event_fd) != 0) {
					printf("Parse Message Error\n");
				}
				p_events.data.fd = -1;
			} else {
				p_events.data.fd = -1;
			}	
		}
	}

	unlink(argv[1]);

	close(server_sockfd);
	/*
	 * close(client_sockfd);	
	*/
	return 0;
}
