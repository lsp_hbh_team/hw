#include "chat_msg.h"
#include "chat_menu.h"

int node_user_kick();
int node_user_print();
int node_user_msg();

struct menu_selection root = {
	.count = 3,
	.menus = {
		{
			.title = "show",
			.func = node_user_print,
		},
		{
			.title = "kick",
			.func = node_user_kick,
		},
		{
			.title = "send_msg",
			.func = node_user_msg,
		},
	},
};
struct menu_selection * curmenu = &root; 
	
int user_count(struct user_record * record)
{
	int count;

	if (record->count < 10) {
		count = record->count;
	} else {
		count = MAXUSER;
	}

	return count;
}

int user_print(struct user_record * record)
{
	int i;
	int count;

	count = user_count(record);

	for (i = 0;i < count;i++) {
	       printf("%d. JOINED ON : %ld LAST ACTIVE : %ld USER SOCKFD : %d ID : %s\n",i+1, record->users[i].join_time, record->users[i].activity, record->users[i].sock_fd, record->users[i].id);
	}	       

	return 0;
}

int node_user_print()
{
	user_print(&record);
	return 0;
}

int user_kick(int client_fd)
{
	if (user_del(&record, client_fd) == -1) {
		printf("ERROR user_del\n");
	}

	if (epoll_ctl(efd, EPOLL_CTL_DEL, client_fd, NULL) == -1) {
		perror("epoll_ctl ");
		return -1;
	}

	close(client_fd);

	return 0;
}

int node_user_kick()
{
	int fd;
	char buf[1024];

	user_print(&record);	
	printf("Kick which user?(Enter -1 to quit) : \n");

	memset(buf, 0, sizeof(buf));
	fgets(buf, sizeof(buf), stdin);
	sscanf(buf, "%d", &fd);

	if (fd != -1) {
		user_kick(fd);
	}

	return 0;
}

int user_msg(int fd, void *buf, int len) 
{
	int ret;
	char *p = buf;
	int saved = 0;

	while(saved < len) {
		ret = send(fd, p + saved, len - saved, 0);

		if (ret == -1) {
			perror("recv ");
			return -1;
		} else if (ret > 0) {
			saved += ret;
			continue;
		} else if (ret == 0) {
			return -1;	
		}
	}
	
	return 0;

}

int bd_msg(char * buf, int sender_fd)
{
	int count = user_count(&record);
	int i, fd, ret;
	struct header client_header;

	client_header.magic = KEY;
	client_header.tag = MESSAGE;
	client_header.length = strlen(buf);

	for (i = 0; i < count; i++) {
		fd = record.users[i].sock_fd;
		if (fd == sender_fd) {
			continue;
		}

		ret = user_msg(fd, &client_header, sizeof(client_header));
		if (ret == -1) {
			printf("Error, user message\n");
			return -1;
		}

		if (user_msg(fd, buf, strlen(buf)) == -1) {
			printf("user_msg error\n");
			return -1;
		}
	}	

	return 0;

}

int parse_msg(char * buf, int sender_fd) {

	int fd, ret, i, j, index;
	int count = 0;
	char user_id[ID_LENGTH];
	char buf2[MAX_MSG];
	char id_list[MAX_MSG];
	char * ptr;	 	

	struct header client_header; 

	ptr = strtok(buf, "#");	
	if (ptr == NULL) {
		printf("ERROR strtok\n");
		return -1;	
	}

	strcpy(id_list, ptr);

	ptr = strtok(NULL, "");
	if (ptr == NULL) {
		printf("ERROR strtok\n");
		return -1;
	}	

	strcpy(buf2, ptr);	

	for (i = 0; i < strlen(id_list); i++) {
		if (id_list[i] == ',') {
			count++;
		}
	}

	client_header.magic = KEY;
	client_header.tag = MESSAGE;
	client_header.length = strlen(buf2);

	if (count == 0) {
		printf("NO COMMAS IN MESSAGE\n");
		if (strcmp(id_list, "bdcast") == 0) {
			printf("BDCASTING\n");
			if (bd_msg(buf2, sender_fd) == -1) {
				printf("bd message error\n");
				return -1;
			}

			return 0;
		}
	}

	for (j = 0; j < count + 1; j++) {
		if (j == 0) {
			ptr = strtok(id_list, ",");
		} else {
			ptr = strtok(NULL, ",");
		}
		if (ptr == NULL) {
			printf("ERROR strtok\n");
			return -1;
		}	
	
		strcpy(user_id, ptr);

		printf("USER_ID : %s\n", user_id);

		index = user_lookup_id(&record, user_id); 

		printf("INDEX VALUE : %d\n", index);
		if (index == -1) {
			printf("USER LOOKUP ID ERROR\n");
			return -1;
		}

		fd = record.users[index].sock_fd;

		printf("USER FD : %d\n", fd);

		if (fd != -1) {
			ret = user_msg(fd, &client_header, sizeof(client_header));
			if (ret == -1) {
				printf("Error, user message\n");
				return -1;
			}

			ret = user_msg(fd, buf2, strlen(buf2));
			if (ret == -1) {
				printf("Error, user message\n");
				return -1;
			}
		}  
	}

	return 0;
}

int node_user_msg() 
{
	char buf[MAX_MSG];

	user_print(&record);	

	printf("Please type in your message : \n");

	memset(buf, 0, sizeof(buf));
	fgets(buf, sizeof(buf), stdin);

	printf("%s\n", buf);

	if (parse_msg(buf, 0) != 0) {
		printf("Parse Message Error\n");
		return -1;
	}

	return 0;

}

int menu_print(struct menu_selection * select)
{
	int i;
	int num = select->count;
	for (i = 0;i < num;i++) {
		printf("%d %s\n", i+1, select->menus[i].title);
	}

	return 0;
}

#if 0
int addchild(int num, struct menu_selection * parent, struct menu_selection * child)
{
	if (num < 0) {
	       return -1;
	}	       

	parent->menus[num].submenu = child;
	child->parent = parent;
}


int menu_return(struct menu_selection ** select)
	
{
	(*select) = (*select)->parent;
}
#endif

int menu_processing(void)
{
	int input;
	char buf[1024];

	memset(buf, 0, sizeof(buf));
	fgets(buf, sizeof(buf), stdin);
	sscanf(buf, "%d", &input);

	if (curmenu->menus[input-1].submenu == NULL) {
		if (curmenu->menus[input-1].func == NULL) {
			printf("ERROR, NO ASSIGNED FUNCTION\n");
			return -1;
		}
		
		if (curmenu->menus[input-1].func(&curmenu) == -1) {
			printf("FUNCTION ERROR(%s()@%s %d line)\n", __func__, __FILE__, __LINE__);
		}

	} else {
		curmenu = curmenu->menus[input-1].submenu;


	}

	menu_print(curmenu);

	return 0;
}
