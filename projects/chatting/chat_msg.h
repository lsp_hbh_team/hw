#ifndef CHAT_MSG_H
#define CHAT_MSG_H

#define KEY 0x12345
#define MESSAGE 0
#define BROADCAST 1
#define REGISTER 2
#define ID_LENGTH 20
#define MAX_MSG 1000
struct header {
	int magic;
	int tag;
	int length;
};

#endif /* chat_msg.h */
