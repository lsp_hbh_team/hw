#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <sys/epoll.h>
#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

int isdir(const char * dirname)
{
	struct stat stats;
	stat(dirname, &stats);

	if (S_ISDIR(stats.st_mode)) {
		return 1;
	} else {
		return 0;
	}
}


int remove_dir(const char * path)
{
	   char * pname;
	   DIR *d = opendir(path);
	   int r = -1;
	   int r2;

	   if (d) {
	      struct dirent *p;

	      r = 0;

	      while (!r && (p = readdir(d))) {
		  r2 = -1;

		  if (!strcmp (p->d_name, ".") || !strcmp (p->d_name, "..")) {
		     continue;
		  }

		   pname = malloc(strlen(path) + strlen(p->d_name) + 2);
	 
		   if (pname == NULL) {
			   printf("ERROR");
			   return -1;
		   }
		  
	          strcpy(pname, path);
		  strcat(pname, "/");
		  strcat(pname, p->d_name);
		  
		  if (pname) {
		     struct stat stats; 

		     if (!stat(pname, &stats)) {
			if (S_ISDIR(stats.st_mode)) {
			   r2 = remove_dir(pname);
			} else {
			   r2 = unlink(pname);
			}
		     }

		     free(pname);
		  }

		  r = r2;
	      }

	      closedir(d);
	   }

	   if (!r) {
	      r = rmdir(path);
	   }

	   return r;
}


int main( int argc, char **argv ) {
	char target[20]; /* monitoring directory name */
	int fd, fd2, efd;
	int wd;
        int ret;	
	char buffer[BUF_LEN];

	const char path1[] = "testdir1";
	const char path2[] = "testdir2";

	struct epoll_event event;

	if (isdir(path1)) {	

		remove_dir(path1);

		printf("isdir true\n");

		if (mkdir(path1, 0700) != 0) {
			perror("mkdir");
			return 1;
		}
	} else {
		printf("isdir else\n");
		if (mkdir(path1, 0700) != 0) {
			perror("mkdir");
			return 1;
		}
	}

	if (isdir(path2)) {	

		remove_dir(path2);

		printf("isdir true\n");

		if (mkdir(path2, 0700) != 0) {
			perror("mkdir");
			return 1;
		}
	} else {
		printf("isdir else\n");
		if (mkdir(path2, 0700) != 0) {
			perror("mkdir");
			return 1;
		}
	}

	fd = inotify_init();
	if (fd < 0) {
		perror("inotify_init");
	}
	wd = inotify_add_watch(fd, path1, IN_CREATE | IN_DELETE);

	fd2 = inotify_init();
	if (fd2 < 0) {
		perror("inotify_init");
	}
	wd = inotify_add_watch(fd2, path2, IN_CREATE | IN_DELETE);

	efd = epoll_create(1);

	event.events = EPOLLIN;
	event.data.fd = fd;
	epoll_ctl(efd, EPOLL_CTL_ADD, fd, &event);

	event.events = EPOLLIN;
	event.data.fd = fd2;
	epoll_ctl(efd, EPOLL_CTL_ADD, fd2, &event);


	while(1) {
		printf("call epoll_wait()\n");
		ret = epoll_wait(efd, &event, 1, -1);	
		
		if (ret == 0) {
			continue;
		} else if (ret == -1) {
			perror("epoll_wait");
			break;
		} else {
		}
		
		printf("RUNNING, fd=%d\n", event.data.fd);
		int length; 
		int i = 0;
		length = read(event.data.fd, buffer, BUF_LEN);
		if (length < 0) {
			perror("read");
		}
		printf("WHILE. length=%d,EVENT_SIZE=%ld\n", length, EVENT_SIZE);
		while (i < length) {
			printf("RUNNING2\n");
			struct inotify_event *event = (struct inotify_event *) &buffer[i];
			printf ("[debug] wd=%d mask=0x%x cookie=%d len=%d dir=%s\n", 
					event->wd, event->mask, event->cookie, event->len, (event->mask & IN_ISDIR)?"yes":"no");
			if (event->len) {
				if (event->mask & IN_CREATE) {
					if (event->mask & IN_ISDIR) {
						printf("The directory %s was created.\n", event->name);      
					} else {
						printf("The file %s was created.\n", event->name);
					}
				} else if (event->mask & IN_DELETE) {
					if (event->mask & IN_ISDIR) {
						printf("The directory %s was deleted.\n", event->name);      
					} else {
						printf("The file %s was deleted.\n", event->name);
					}
				}
			}
			i += EVENT_SIZE + event->len;
		}
	}

     inotify_rm_watch(fd, wd);
     close(fd);

     return 0;
}