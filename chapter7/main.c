#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#define MAX 10000

unsigned int counter = 0;
int thread1 = 0;
int thread2 = 0;
int done1 = 0;
int done2 = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void * add1 ()
{		
	int num = MAX/10;
	long retval;

	while (counter <= MAX && (thread1 + thread2*3) <= 10000) {
		if (thread1 <= num){
			pthread_mutex_lock(&mutex);
			counter+= 1;
			thread1++;
			pthread_mutex_unlock(&mutex);
			usleep(3);
		} else {
		}
	}
	

	printf("Thread 1 : %d\n", thread1);
	pthread_exit((void *)retval);
	

}

void * add3 () 
{
	int num = MAX/10;
	long retval;

	while (counter <= MAX && (thread1 + thread2*3) <= 10000) {
		if (thread2 <= num*3) {
			pthread_mutex_lock(&mutex);
			counter += 3;
			thread2++;
			pthread_mutex_unlock(&mutex);
			usleep(1);
		} else {
		}
	}

	printf("Thread 2 : %d\n", thread2);
	pthread_exit((void *)retval);

}

int main()
{
	int tid;
	pthread_t p_thread[2];
	int ret;
	long status;

	tid = pthread_create(&p_thread[0], NULL, add1, NULL);

	if (tid != 0) {
		perror("pthread create");
		goto err;
	}

	tid = pthread_create(&p_thread[1], NULL, add3, NULL);

	if (tid != 0) {
		perror("pthread create");
		goto err;
	}

	ret = pthread_join(p_thread[0], (void *) status);

	 if (ret != 0) {
		perror("pthread join");
		printf("STATUS : %ld\n", status);
		goto err;
	 }

	ret = pthread_join(p_thread[1], (void *) status);

	if (ret != 0) {
		perror("pthread join");
		printf("STATUS : %ld\n", status);
		goto err;
	}

	ret = pthread_mutex_destroy(&mutex);

	if (ret != 0) {
		perror("mutex destroy");
		printf("STATUS : %ld\n", status);
		goto err;
	}

	printf("COUNTER : %d\n", counter);

err: {
	pthread_mutex_destroy(&mutex);
   	return -1;
     }

	return 0;
}
