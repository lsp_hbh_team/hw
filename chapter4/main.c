#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>

struct info {
	char name[16];
	int age;
	int phone_num;
};

/*
 * Returns size of file in bytes.
 */
int getsize(char *filename) 
{
	FILE *fp;
	int size;
	
	fp = fopen(filename, "r");
	if (!fp) {
		perror("fopen");
		return -1;
	}

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);

	rewind(fp);

	fclose(fp);

	return size;
}

/*
 * Prints out all member variables for every struct info saved in
 * the file.
 */

void print_all(void)
{
	FILE *fp = NULL;
	struct info p;

	if ((fp = fopen("data", "r")) == NULL) {
		printf("fopen(r) fail\n");
		
	}

	while (feof(fp) == 0) {
		if (!fread(&p, sizeof(struct info), 1, fp)) {
			break;
		}
		printf("Name : %s Age : %d Phone Number : %d\n", 
				p.name, p.age, p.phone_num);
	}

	rewind(fp);

	if (fp) {
		fclose(fp);
	}
}

int append(int fd, struct info * new)
{
	struct info *out;
	int ssize = sizeof(struct info);
	int fsize = getsize("data");
	int num_entries;

	lseek(fd, 0, SEEK_SET);
	
	if (ftruncate(fd, fsize + ssize) == -1) {
		perror("ftruncate");
		return -1;
	}	


	num_entries = fsize / ssize;
	fsize += ssize;
	
	out = (struct info *) mmap(0, fsize, PROT_WRITE, MAP_SHARED, fd, 0);
	if (!out) {
		perror("mmap");
		if (munmap(out, fsize) == -1) {
			perror("munmap");
			return -1;
		}
		return -1;
	}
	memcpy(&out[num_entries], new, ssize);

	msync(out, ssize, MS_SYNC);
	if (munmap(out, ssize) == -1) {
		perror("munmap");
		return -1;
	}

	return 0;
}


int main()
{		
	struct info new;
	int fd, fsize;
	int preexist = 0;
	int ssize = sizeof(struct info);
	struct info *out;
	int num_entries;
	int i;

	printf("Please enter name : ");
	scanf("%s", new.name);
	printf("Please enter age : ");
	scanf("%d", &new.age);
	printf("Please enter phone number : ");
	scanf("%d", &new.phone_num);

	fd = open ("data", O_CREAT, 0700);
	if (fd == -1) {
		perror ("open");
		goto err;
	}
	
	close(fd);

	fsize = getsize("data");
	num_entries = fsize / ssize;

	fd = open("data", O_RDWR);
	if (fd == -1) {
		perror("open");
		goto err;
	}
	
	/* Checks if data is empty. If true, writes user input via mmap.
	 * Else, checks if a struct with the same name exists, and replaces
	 * it if true.
	 */
	if (fsize == 0) {
		if (append(fd, &new)) {
			printf("append() fail\n");
		}
	} else {
		out = (struct info *) mmap(0, ssize * num_entries, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		if (!out) {
			perror("mmap()\n");
			goto err;
		}
		for (i = 0; i < num_entries; i++) {
			if (strcmp(out[i].name, new.name) == 0) {
				preexist = 1;	
				memcpy(&out[i], &new, ssize);
				break;
			}
		}

		msync(out, ssize, MS_SYNC);
		if (munmap(out, ssize) == -1) {
			perror("munmap");
			goto err;
		}

		/* Appends a new struct to the file*/
		if (preexist == 0) {
			append(fd, &new);
		}
	}		

	close(fd);

	print_all();

	return 0;

err:
	if (fd) {
		close(fd);
	}
	return -1;
}

