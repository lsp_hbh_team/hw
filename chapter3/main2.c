#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

#define BUFF_SIZE 32

int getsize(char *filename) {
        FILE *fp;
        int size;
        
        fp = fopen(filename, "r");
        if (!fp) {
                perror("fopen");
#if 0
                return 1;
#else
		return -1;
#endif
        }

        fseek(fp, 0, SEEK_END);
        size = ftell(fp);

#if 0
	/* no meaning... */
        rewind(fp);
#endif

        fclose(fp);

        return size;
}

int main(int argc, char **argv)
{
        FILE *fp;
        int fd;
        int fsize;
        int remain;
        char buff[BUFF_SIZE];
        struct stat sb;
        
        if (argc != 2) {
                printf("ERROR, WRONG INPUT");
                return 1;
        }

#if 0
        fsize = getsize(argv[1]);
#else
        if ((fsize = getsize(argv[1])) < 0) {
		printf("getting size fail\n");
		return -1;
	}
#endif

        fp = fopen(argv[1], "r+");

        if (!fp) {
                perror("fopen");
                return 1;
        }

        fd = fileno(fp);

        if (fstat(fd, &sb)) {
                printf("fstat() error\n");
                goto error;
        }

        if (!S_ISREG(sb.st_mode)) {
                printf("WRONG FILE\n");
                goto error;
        }

        while (feof(fp) == 0) {
                if ((fsize - ftell(fp)) >= BUFF_SIZE) {
                        fread(buff, BUFF_SIZE, 1, fp);
                        fwrite(buff, BUFF_SIZE, 1, stdout);
                } else {
                        memset(buff, 0, BUFF_SIZE);
                        remain = fsize - ftell(fp);
                        fread(buff, remain, 1, fp);
                        fwrite(buff, remain, 1, stdout);
#if 0
			/* mistake? ;) */
                        printf("\n%d\n", feof(fp));
#endif
                        break;
                }
        }

        fclose(fp);

        return 0;

error:
        if (fp) {
                fclose(fp);
        }
        return 1;
}

